This application is a variation of the RUSA ACP controle time calculator.

The difference between this implementation and that one is that we will be using 
AJAX to automatically populate the appropriate open and close times of a control
on a given brevet, and also provided a starting date and time by the user.

The options for brevet distances are 200km, 300km, 400km, 600km, and 1000km.

Uses arrow library to alter and display the date and time string format
back and forth between frontend and backend implementations.

Times are altered according to the rules already put in place by the organization and
made available to the rest of the public.

Adrian Sierra
asierra4@uoregon.edu
